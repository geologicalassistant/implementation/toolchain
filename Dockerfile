FROM openjdk:8-slim-buster

ADD target/geoassistant-impl-0.1-SNAPSHOT.jar /usr/geo/geo.jar


# Maude
RUN apt-get update && apt-get install -y \
    maude

ADD software/sis/* /usr/geo/maude/sis/
ADD software/js/* /usr/geo/maude/js/


# Python
RUN apt-get update && apt-get install -y \
    git \
    python3 \
    nano

ADD software/python/expansion_script.py /usr/geo/python/expansion_script.py
ADD software/python/UnknownReplacer.py /usr/geo/python/UnknownReplacer.py


# Prolog
RUN apt-get -y install swi-prolog

ADD software/prolog/timerizer.pl /usr/geo/prolog/timerizer.pl
ADD software/prolog/timing/* /usr/geo/prolog/timing/
ADD software/prolog/parser/* /usr/geo/prolog/parser/


WORKDIR /usr/geo

ENTRYPOINT ["java", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-Ddocker=true", "-jar", "geo.jar"]