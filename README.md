# GeoAssistant Pipeline
This is the implementation of _GeoAssistant_'s tool-chain which combines all functionality and tools together into one continuous pipeline.

## System Requirements and Usage Information
The pipeline relies on a pre-existing relational database system to store the simulation data and results.

For configuration options regarding the database, URLs or ports, see _application.properties_ in _/src/main/ressources/_.

The pipeline can be split to run on several machines or containers and is an use-case implementation of https://github.com/Sirius-sfi/computation-framework.

A Dockerfile and different Docker-Compose files can be found in the project root, a database-specific Dockerfile and script in _/db-config-docker/_.

To start a pipeline-run, a properly formatted initial configuration file must be enclosed in a request to:
http://#controller-IP#:#controller-port#/startComputation
