-- expecting am existing database 'geo'
alter database geo owner to postgres;


create schema geo;
alter schema geo owner to postgres;


create table geo.computation_run
(
    id               bigserial not null
        constraint computation_run_pkey
            primary key,
    initial_config      varchar   not null,
    avgwp               bigint,
    computing_time      bigint,
    fastestwp           bigint,
    preparation_time    bigint,
    slowestwp           bigint,
    number_nodes_end    integer,
    number_nodes_start  integer,
    numberwps           integer
);
alter table geo.computation_run owner to postgres;


create table geo.final_configuration
(
	id bigserial not null
		constraint final_configuration_pkey
			primary key,
	final_config varchar not null,
	computation_run_id bigint
		constraint fk4ctnosnxd0y9x4hjn4tj67u8b
			references geo.computation_run
);
alter table geo.final_configuration owner to postgres;
