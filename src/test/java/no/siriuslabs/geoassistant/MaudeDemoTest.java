package no.siriuslabs.geoassistant;

import no.siriuslabs.computationapi.implementation.util.ShellHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static no.siriuslabs.geoassistant.GeoAssistantImplementationController.MAUDE_QUIT_COMMAND;

public class MaudeDemoTest {

	private static GeoAssistantImplementationController controller;
	private static String executable;
	private static String maudeCall;

	@BeforeAll
	public static void init() {
		controller = new GeoAssistantImplementationController(null, null, null);

		executable = controller.getSysDependentMaudeExecutable();
		maudeCall = controller.getMaudePath() + executable;
	}

	@Test
	public void testMaudeVersion() {
		List<String> maudeParameters = Arrays.asList();
		
		final List<String> followUpCommands = new ArrayList<>();
		followUpCommands.add(MAUDE_QUIT_COMMAND);

		String result = ShellHelper.runShellCommand(maudeCall, maudeParameters, followUpCommands.toArray(new String[0]));

		Assertions.assertTrue(result.contains("Maude 3"));
	}

	@Test
	public void testSisErrorMessage() {
		List<String> maudeParameters = Arrays.asList(controller.getMaudeProgramPath() + controller.getMaudeUsecaseSubdir(Usecase.USECASE_1));

		final List<String> followUpCommands = new ArrayList<>();
		followUpCommands.add(MAUDE_QUIT_COMMAND);

		String result = ShellHelper.runShellCommand(maudeCall, maudeParameters, followUpCommands.toArray(new String[0]));

		Assertions.assertTrue(result.contains("Warning: <command line>: unable to locate file:"));
	}

}
