package no.siriuslabs.geoassistant.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * JPA repository to handle FinalConfiguration instances.
 */
@Repository("finalConfigurationRepository")
public interface FinalConfigurationRepository extends JpaRepository<FinalConfiguration, Long> {

}
