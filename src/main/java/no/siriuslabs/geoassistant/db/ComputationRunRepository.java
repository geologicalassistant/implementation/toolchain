package no.siriuslabs.geoassistant.db;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * JPA repository to handle ComputationRun instances.
 */
@Repository("computationRunRepository")
public interface ComputationRunRepository extends JpaRepository<ComputationRun, Long> {

}
