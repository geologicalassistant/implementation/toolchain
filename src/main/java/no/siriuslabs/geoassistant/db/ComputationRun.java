package no.siriuslabs.geoassistant.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class used to persist information about a computation run into a database.
 * The information includes the initial configuration as received with the ComputationRequest and various statistical data about the run itself.
 */
@Entity
@Table(name = "computation_run")
public class ComputationRun {

	/**
	 * Unique technical ID of the computation run.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	/**
	 * Initial Maude configuration as received with the ComputationRequest.
	 */
	@Column(name = "initial_config", columnDefinition = "VARCHAR", nullable = false)
	private String initialConfig;

	/**
	 * Overall running time from start to finish.
	 */
	@Column(name = "computingTime", columnDefinition = "BIGINT", nullable = true)
	private long computingTime;

	/**
	 * Time taken for the prepareAndPackageData phase.
	 */
	@Column(name = "preparationTime", columnDefinition = "BIGINT", nullable = true)
	private long preparationTime;

	/**
	 * Number of worker nodes available after the WorkPackages were created.
	 */
	@Column(name = "numberNodesStart", columnDefinition = "INT", nullable = true)
	private int numberNodesStart;

	/**
	 * Number of worker nodes available when accumulating the results.
	 */
	@Column(name = "numberNodesEnd", columnDefinition = "INT", nullable = true)
	private int numberNodesEnd;

	/**
	 * Number of unique WorkPackages computed.
	 */
	@Column(name = "numberWPs", columnDefinition = "INT", nullable = true)
	private int numberWPs;

	/**
	 * Shortest time taken to compute a WorkPackage.
	 */
	@Column(name = "fastestWP", columnDefinition = "BIGINT", nullable = true)
	private long fastestWP;

	/**
	 * Longest time taken to compute a WorkPackage.
	 */
	@Column(name = "slowestWP", columnDefinition = "BIGINT", nullable = true)
	private long slowestWP;

	/**
	 * Average time taken to compute a WorkPackage.
	 */
	@Column(name = "avgWP", columnDefinition = "BIGINT", nullable = true)
	private long avgWP;

	public long getId() {
		return id;
	}

	public String getInitialConfig() {
		return initialConfig;
	}

	public void setInitialConfig(String initialConfig) {
		this.initialConfig = initialConfig;
	}

	public long getComputingTime() {
		return computingTime;
	}

	public void setComputingTime(long computingTime) {
		this.computingTime = computingTime;
	}

	public long getPreparationTime() {
		return preparationTime;
	}

	public void setPreparationTime(long preparationTime) {
		this.preparationTime = preparationTime;
	}

	public int getNumberNodesStart() {
		return numberNodesStart;
	}

	public void setNumberNodesStart(int numberNodesStart) {
		this.numberNodesStart = numberNodesStart;
	}

	public int getNumberNodesEnd() {
		return numberNodesEnd;
	}

	public void setNumberNodesEnd(int numberNodesEnd) {
		this.numberNodesEnd = numberNodesEnd;
	}

	public int getNumberWPs() {
		return numberWPs;
	}

	public void setNumberWPs(int numberWPs) {
		this.numberWPs = numberWPs;
	}

	public long getFastestWP() {
		return fastestWP;
	}

	public void setFastestWP(long fastestWP) {
		this.fastestWP = fastestWP;
	}

	public long getSlowestWP() {
		return slowestWP;
	}

	public void setSlowestWP(long slowestWP) {
		this.slowestWP = slowestWP;
	}

	public long getAvgWP() {
		return avgWP;
	}

	public void setAvgWP(long avgWP) {
		this.avgWP = avgWP;
	}
}
