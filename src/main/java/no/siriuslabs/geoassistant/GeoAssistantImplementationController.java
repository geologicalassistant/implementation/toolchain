package no.siriuslabs.geoassistant;

import com.geoassistant.scenariogen.OntologyPermuter;
import no.siriuslabs.computationapi.api.model.computation.*;
import no.siriuslabs.computationapi.api.model.request.ComputationRequest;
import no.siriuslabs.computationapi.api.model.request.Payload;
import no.siriuslabs.computationapi.implementation.AbstractImplementationController;
import no.siriuslabs.computationapi.implementation.config.ConfigProperties;
import no.siriuslabs.computationapi.implementation.util.ShellHelper;
import no.siriuslabs.geoassistant.db.ComputationRun;
import no.siriuslabs.geoassistant.db.ComputationRunRepository;
import no.siriuslabs.geoassistant.db.FinalConfiguration;
import no.siriuslabs.geoassistant.db.FinalConfigurationRepository;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static no.siriuslabs.geoassistant.Usecase.JOHAN_SVERDRUP;
import static no.siriuslabs.geoassistant.Usecase.USECASE_1;

/**
 * Rest controller implementation for the GeoAssistant application.<p>
 * This contains the domain specific worker node API implementation required for ImplementationControllers for the GeoAssistant domain.<p>
 * The prepare and the computation steps call external software to run the actual logic and collect the results afterwards.
 */
@RestController
public class GeoAssistantImplementationController extends AbstractImplementationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(GeoAssistantImplementationController.class);

	protected static final String MAUDE_EXECUTABLE_MAC = "maude-CVC4.darwin64";
	protected static final String MAUDE_EXECUTABLE_LINUX = "maude";

	// TODO move paths and filenames to config later
	protected static final String MAUDE_PATH_LOCAL = "./software/Maude3/";
	protected static final String MAUDE_PROGRAM_PATH_LOCAL = "./software/";
	protected static final String MAUDE_PATH_DOCKER = "";
	protected static final String MAUDE_PROGRAM_PATH_DOCKER = "./maude/";

	protected static final String MAUDE_USECASE_SUBDIR_SIS = "sis/";
	protected static final String MAUDE_USECASE_SUBDIR_JS = "js/";

	private static final String JS_HISTORY_FILENAME_1 = "history.txt";
	private static final String JS_HISTORY_FILENAME_2 = "detailedHistory.txt";

	protected static final String PYTHON_PATH_LOCAL = "./software/python/";
	protected static final String PYTHON_PATH_DOCKER = "/usr/geo/python/";

	protected static final String PYTHON_EXECUTABLE = "python3";
	protected static final String PYTHON_SCRIPT_NAME = "expansion_script.py";
	protected static final String CONFIG_FILENAME = "geo-init.maude";
	protected static final String PYTHON_CONFIG_DIRECTORY = "/configs";
	protected static final String PYTHON_CONFIG_FILE_PATH = '.' + PYTHON_CONFIG_DIRECTORY + '/' + CONFIG_FILENAME;
	protected static final String OUTPUT_DIRECTORY = "/outputs";
	protected static final String PYTHON_OUTPUT_PARAMETER = "outputs";

	protected static final String PROLOG_EXECUTABLE = "swipl";
	protected static final String PROLOG_SCRIPT_PATH = "./../prolog/timerizer.pl";
	protected static final String PROLOG_SRC_FILENAME = "geo-init-src.maude";
	protected static final String PROLOG_SRC_FILE_PATH = '.' + PYTHON_CONFIG_DIRECTORY + '/' + PROLOG_SRC_FILENAME;

	protected static final String FILENAME_ATTRIBUTE_KEY = "filename";
	protected static final String CONFIG_FILENAME_ATTRIBUTE_KEY = "configFilename";
	protected static final String CONFIG_DATA_ATTRIBUTE_KEY = "configData";
	protected static final String QUERIES_ATTRIBUTE_KEY = "queries";
	protected static final String INITIAL_CONFIG_ATTRIBUTE_KEY = "initialConfig";

	protected static final String MAUDE_QUIT_COMMAND = "quit";
	protected static final String MAUDE_FILE_SUFFIX = ".maude";
	protected static final String QUERY_CASE_STUDY_WILDCARD = "%scenario%";

	protected static final String COMMAND_RESULT_ATTRIBUTE_KEY = "commandResult";

	private static final Pattern CASE_STUDY_WILDCARD_PATTERN = Pattern.compile(QUERY_CASE_STUDY_WILDCARD, Pattern.LITERAL);

	private static final String USE_CASE_ATTRIBUTE_KEY = "useCase";
	private static final String RE_MATERIALIZE_COMMENT_TEXT = " re-materialized for initial config";

	private static final String WORKING_DIR = System.getProperty("user.dir");

	private static final String INPUT_ONTOLOGY_FILENAME = "input.owl";
	private static final String JAVA_OUTPUT_DIRECTORY_NAME = "/output";
	private static final String PERMUTER_OUTPUT_PATH_STUB = JAVA_OUTPUT_DIRECTORY_NAME + "/output";
	private static final String OUTPUT_DIRECTORY_PATH = WORKING_DIR + JAVA_OUTPUT_DIRECTORY_NAME;

	/**
	 * ComputationRun JPA repository.
	 */
	private final ComputationRunRepository computationRunRepository;
	/**
	 * FinalConfiguration JPA repository.
	 */
	private final FinalConfigurationRepository finalConfigurationRepository;

	/**
	 * Autowired constructor.
	 */
	@Autowired
	public GeoAssistantImplementationController(ConfigProperties configProperties, ComputationRunRepository computationRunRepository, FinalConfigurationRepository finalConfigurationRepository) {
		super(configProperties);
		this.computationRunRepository = computationRunRepository;
		this.finalConfigurationRepository = finalConfigurationRepository;
	}

	/**
	 * Implements the data validation step of the GeoAssistant application.<p>
	 * The currently does not do any data checks.
	 * @return A list of error messages or an empty list if no errors were found.
	 */
	@Override
	@PostMapping("/validateData")
	public ResponseEntity<List<String>> validateData(@RequestBody Payload payload) {
		LOGGER.info("Received data package for validation: {}", payload);

		List<String> messages = new ArrayList<>();


		// TODO


		LOGGER.info("Validation finished. {} messages", messages.size());

		return ResponseEntity.ok(messages);
	}

	/**
	 * Implements the data preparation and WorkPackage generation step of the GeoAssistant application.<p>
	 * This method takes the initial Maude configuration from the ComputationRequest, saves it into a file and runs the Prolog timing tool and the Python scenario expansion tool on that configuration file.
	 * All the resulting proto-scenario files are read into WorkPackages and returned as result. The initial configuration is also persisted to the database.
	 * @return The list of WorkPackages generated from the data in the ComputationRequest.
	 */
	@Override
	@PostMapping("/prepareAndPackageData")
	public ResponseEntity<List<WorkPackage>> prepareAndPackageData(@RequestBody ComputationRequest request) {
		LOGGER.info("Received data package for preparation: {}", request);

		boolean dockerActive = isDockerActive();
		LOGGER.info("Docker-mode={}", dockerActive);

		List<WorkPackage> results;
		String useCase = (String) request.getPayload().getData().get(USE_CASE_ATTRIBUTE_KEY);
		if(useCase == null || Usecase.USECASE_1 == Usecase.valueOf(useCase)) {
			LOGGER.info("Usecase flag is {} - going with Python expander", useCase);
			results = runPythonScenarioExp(request, dockerActive);
		}
		else if(JOHAN_SVERDRUP == Usecase.valueOf(useCase)) {
			try {
				LOGGER.info("Usecase flag is {} - going with Java expander",  useCase);
				results = runJavaScenarioExp(request);
			}
			catch(IOException | OWLOntologyCreationException e) {
				LOGGER.error(e.getMessage(), e);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ArrayList<>());
			}
		}
		else {
			// should never happen
			LOGGER.warn("No matching usecase found!");
			results = new ArrayList<>();
		}

		// constrain scenarios to 5 for testing
//		results = results.subList(0, 5);

		LOGGER.info("Preparation finished.");
		return ResponseEntity.ok(results);
	}

	private List<WorkPackage> runPythonScenarioExp(@RequestBody ComputationRequest request, boolean dockerActive) {
		String pythonHomeDir = "";
		String processWorkingDir = "";
		if(dockerActive) {
			pythonHomeDir = PYTHON_PATH_DOCKER;
			processWorkingDir = PYTHON_PATH_DOCKER;
		}
		else {
			pythonHomeDir = PYTHON_PATH_LOCAL;
			processWorkingDir = PYTHON_PATH_LOCAL.substring(2);
		}
		LOGGER.info("Python home directory: {}", pythonHomeDir);

		LOGGER.info("Preparing Python environment");
		cleanupPythonFiles(pythonHomeDir);
		placeInitialConfigFile(pythonHomeDir, request);

		// run Prolog timing tool
		List<String> prologParameters = Arrays.asList("-s", PROLOG_SCRIPT_PATH, PROLOG_SRC_FILE_PATH, PYTHON_CONFIG_FILE_PATH);
		ShellHelper.runShellCommand(new File(processWorkingDir), PROLOG_EXECUTABLE, prologParameters);

		// then run Python scenario expansion - uses the same config file as Prolog, so no copying needed
		List<String> pythonParameters = Arrays.asList(PYTHON_SCRIPT_NAME, PYTHON_CONFIG_FILE_PATH, PYTHON_OUTPUT_PARAMETER);
		ShellHelper.runShellCommand(new File(processWorkingDir), PYTHON_EXECUTABLE, pythonParameters);

		ComputationRun run = initializeComputationRun(request);

		String outputDirectoryPath = pythonHomeDir + OUTPUT_DIRECTORY;
		return buildWorkPackages(request, outputDirectoryPath, run);
	}

	private ComputationRun initializeComputationRun(@RequestBody ComputationRequest request) {
		ComputationRun run = new ComputationRun();
		final String initialConfig = (String) request.getPayload().getData().get(INITIAL_CONFIG_ATTRIBUTE_KEY);
		run.setInitialConfig(formatMaudeConfigString(initialConfig));
		run = computationRunRepository.save(run);
		return run;
	}

	private List<WorkPackage> runJavaScenarioExp(@RequestBody ComputationRequest request) throws IOException, OWLOntologyCreationException {
		cleanupJavaFiles();

		List<String> lines = reconvertDataAttributeFromJson(request, MaudeFileConverter.DataType.ONTOLOGY);
		LOGGER.info("Preparing input ontology");
		Files.write(Paths.get(INPUT_ONTOLOGY_FILENAME), lines, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);

		LOGGER.info("Setting-up permuter");
		OntologyPermuter op = new OntologyPermuter(false);
		op.initialize(INPUT_ONTOLOGY_FILENAME, PERMUTER_OUTPUT_PATH_STUB);
		LOGGER.info("Permuter starts");
		op.permute();
		LOGGER.info("Permuter finished");

		ComputationRun run = initializeComputationRun(request);
		List<WorkPackage> results = buildWorkPackages(request, OUTPUT_DIRECTORY_PATH, run);
		LOGGER.info("{} results collected and prepared", results.size());

		cleanupJavaFiles();

		return results;
	}

	private void cleanupJavaFiles() throws IOException {
		LOGGER.info("Cleaning-up Java environment");
		if(Files.notExists(Paths.get(OUTPUT_DIRECTORY_PATH))) {
			LOGGER.info("{} does not exist - creating", OUTPUT_DIRECTORY_PATH);
			Files.createDirectory(Paths.get(OUTPUT_DIRECTORY_PATH));
		}

		LOGGER.info("Deleting input ontology: {}", INPUT_ONTOLOGY_FILENAME);
		Files.deleteIfExists(Paths.get(INPUT_ONTOLOGY_FILENAME));
		LOGGER.info("Clearing output directory: {}", OUTPUT_DIRECTORY_PATH);
		deleteFilesInDirectory(OUTPUT_DIRECTORY_PATH);
	}

	/**
	 * Deletes files related to the Python tool in the config and output directories as well as the default named initial config file relative to the given directory.
	 */
	private void cleanupPythonFiles(String pythonHomeDir) {
		LOGGER.info("Cleaning-up Python environment");
		File configInPythonRoot = new File(pythonHomeDir + '/' + CONFIG_FILENAME);
		if(configInPythonRoot.exists()) {
			boolean res = configInPythonRoot.delete();
			LOGGER.info("{}" + CONFIG_FILENAME + " deleted={}", pythonHomeDir, res);
		}

		String outputDirectoryPath = pythonHomeDir + OUTPUT_DIRECTORY;
		LOGGER.info("Output directory for scenarios: {}", outputDirectoryPath);
		deleteFilesInDirectory(outputDirectoryPath);

		String configDirectoryPath = pythonHomeDir + PYTHON_CONFIG_DIRECTORY;
		LOGGER.info("Directory for configurations: {}", configDirectoryPath);
		deleteFilesInDirectory(configDirectoryPath);
	}

	/**
	 * Deletes all files found in the given directory. Subdirectories are not recognized.
	 */
	private void deleteFilesInDirectory(String directoryPath) {
		final File directory = new File(directoryPath);
		if(directory.exists()) {
			int counter = 0;
			int fileCount = directory.listFiles().length;
			LOGGER.info("Found {} files in {}", fileCount, directory);

			for(File file : directory.listFiles()) {
				boolean res = file.delete();
				if(res) {
					counter++;
				}
			}

			LOGGER.info("Deleted {} files", counter);
		}
	}

	/**
	 * Saves the initial configuration given in the ComputationRequest to the Python config directory relative to the given path.<p>
	 * The config directory will be created, should it not exist.
	 * The initial configuration will be converted back from JSON to Maude format (re-adding several protected characters) before saving to file.
	 */
	private void placeInitialConfigFile(String pythonHomeDir, @RequestBody ComputationRequest request) {
		LOGGER.info("Preparing initial config file");
		String configDirectoryPath = pythonHomeDir + PYTHON_CONFIG_DIRECTORY;

		final File configDirectory = new File(configDirectoryPath);
		if(!configDirectory.exists()) {
			LOGGER.info("Directory {} does not exist", configDirectoryPath);
			boolean res = configDirectory.mkdir();
			LOGGER.info("Created directory {} ={}", configDirectory, res);
		}

		try {
			List<String> lines = reconvertDataAttributeFromJson(request, MaudeFileConverter.DataType.MAUDE);

			final String configFilePath = pythonHomeDir + PYTHON_CONFIG_DIRECTORY + '/' + PROLOG_SRC_FILENAME;
			LOGGER.info("Writing config file: {}", configFilePath);
			Path configFile = Paths.get(configFilePath);
			Files.write(configFile, lines, StandardCharsets.UTF_8);
		}
		catch(IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	private List<String> reconvertDataAttributeFromJson(@RequestBody ComputationRequest request, MaudeFileConverter.DataType dataType) {
		String configData = (String) request.getPayload().getData().get(INITIAL_CONFIG_ATTRIBUTE_KEY);

		List<String> lines = MaudeFileConverter.convertJsonToMaude(configData);
		if(MaudeFileConverter.DataType.MAUDE == dataType) {
			lines.add(0, "---" + RE_MATERIALIZE_COMMENT_TEXT); // add a comment to recognize the file was changed
		}
		else if(MaudeFileConverter.DataType.ONTOLOGY == dataType) {
//			lines.add(0, "#" + RE_MATERIALIZE_COMMENT_TEXT); // add a comment to recognize the file was changed
		}
		return lines;
	}

	/**
	 * Converts the given String from JSON to Maude compatible format, adds line separators in the right places and returns the modified String.
	 */
	private String formatMaudeConfigString(String initialConfig) {
		List<String> list = MaudeFileConverter.convertJsonToMaude(initialConfig);
		StringBuilder builder = new StringBuilder();
		for(String s : list) {
			builder.append(s);
			builder.append(System.lineSeparator());
		}
		return builder.toString();
	}

	/**
	 * Creates WorkPackages from the proto-scenario files generated by the Python tool.<p>
	 * The contents of each file will be converted from Maude to a JSON-usable format (removal/escaping of several protected characters) and encapsulated in a WorkPackage instance.
	 * @param request       	ComputationRequest which caused this computation run - needed to create the WorkPackage.
	 * @param outputDirectory	Directory the previously generated files are residing in.
	 * @param run           	ComputationRun this operation belongs to - needed to create the WorkPackage.
	 * @return					A list of WorkPackages containing the contents of all generated files.
	 */
	private List<WorkPackage> buildWorkPackages(@RequestBody ComputationRequest request, String outputDirectory, ComputationRun run) {
		LOGGER.info("Collecting generated scenarios in {}", outputDirectory);

		List<WorkPackage> results = new ArrayList<>();

		try(Stream<Path> paths = Files.walk(Paths.get(outputDirectory))) {
			paths.forEach((Path filePath) -> {
				if (Files.isRegularFile(filePath)) {
					try {
						int fileSize = (int) Files.size(filePath);
						List<String> lines = Files.readAllLines(filePath, StandardCharsets.UTF_8);
						lines = MaudeFileConverter.convertMaudeToJson(lines);

						StringBuilder fileContents = new StringBuilder(fileSize);
						for(String s : lines) {
							fileContents.append(s);
						}

						WorkPackage wp = createWorkPackage(request, filePath, fileContents.toString(), run);

						results.add(wp);
					}
					catch(IOException e) {
						LOGGER.error(e.getMessage(), e);
					}
				}
			});
		}
		catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
		return results;
	}

	/**
	 * Creates a WorkPackage instance from the given data.
	 * @param request		ComputationRequest which caused this computation run.
	 * @param filePath		Name of the output file that contained this proto-scenario.
	 * @param fileContents  File contents - must already be converted to a JSON-usable format.
	 * @param run           ComputationRun this operation belongs to.
	 * @return				A single WorkPackage containing the file's contents and additional information and references.
	 */
	private WorkPackage createWorkPackage(@RequestBody ComputationRequest request, Path filePath, String fileContents, ComputationRun run) {
		WorkPackage wp = new WorkPackage(request.getDomain(), getNextWorkPackageId());
		wp.setRunId(run.getId());
		Map<String, Object> dataMap = new HashMap<>();
		dataMap.put(CONFIG_FILENAME_ATTRIBUTE_KEY, filePath.getFileName().toString());
		dataMap.put(CONFIG_DATA_ATTRIBUTE_KEY, fileContents);
		dataMap.put(USE_CASE_ATTRIBUTE_KEY, request.getPayload().getData().get(USE_CASE_ATTRIBUTE_KEY));
		wp.setData(dataMap);

		return wp;
	}

	/**
	 * Implements the computation step of the GeoAssistant application.<p>
	 * The method first writes the proto-scenario contained in the WorkPackage to a file and then runs the Maude program aimed at this configuration file with the
	 * command(s) given in the WorkPackage.<p>
	 * After finishing, the resulting final configuration is taken from the shell and returned as a WorkPackageResult as well as persisted in the database.
	 * @return A WorkPackageResult containing the final configuration from the simulation.
	 */
	@Override
	@PostMapping("/runComputation")
	public ResponseEntity<WorkPackageResult> runComputation(@RequestBody WorkPackage workPackage) {
		LOGGER.info("Received data package for computation: {}", workPackage);

		placeConfigFile(workPackage);

		String commandResult = runMaudeProgram(workPackage);

		WorkPackageResult result = new WorkPackageResult(workPackage);
		Map<String, Object> resultMap = new HashMap<>();
		resultMap.put(COMMAND_RESULT_ATTRIBUTE_KEY, commandResult);
		result.setData(resultMap);

		FinalConfiguration finalConfiguration = new FinalConfiguration();
		finalConfiguration.setComputationRun(computationRunRepository.getOne(workPackage.getRunId()));
		finalConfiguration.setFinalConfig(commandResult);
		finalConfigurationRepository.save(finalConfiguration);

		LOGGER.info("Computation finished. Returned result: {}", result);
		return ResponseEntity.ok(result);
	}

	/**
	 * Saves the proto-scenario contained in the given WorkPackage to a Maude configuration file in the Maude program's directory.<p>
	 * The data will be converted from JSON to Maude compatible format before writing it to the file.
	 * If a file with this name already exists, it will be deleted.
	 */
	private void placeConfigFile(WorkPackage workPackage) {
		try {
			String usecaseAttribute = (String) workPackage.getData().get(USE_CASE_ATTRIBUTE_KEY);
			Usecase usecase = usecaseAttribute == null || usecaseAttribute.trim().isEmpty() ? null : Usecase.valueOf(usecaseAttribute);

			if(JOHAN_SVERDRUP == usecase) {
				// delete old history files
				String directory = getMaudeProgramPath() + getMaudeUsecaseSubdir(usecase);
				deleteFile(directory + JS_HISTORY_FILENAME_1);
				deleteFile(directory + JS_HISTORY_FILENAME_2);
			}
			String configFileInMaude = getMaudeProgramPath() + getMaudeUsecaseSubdir(usecase) + CONFIG_FILENAME;
			deleteFile(configFileInMaude);

			List<String> lines = MaudeFileConverter.convertJsonToMaude((String) workPackage.getData().get(CONFIG_DATA_ATTRIBUTE_KEY));
			lines.add(0, "--- re-materialized for WP config"); // add a comment to recognize the file was changed

			Path configFilePath = Paths.get(configFileInMaude);
			Files.write(configFilePath, lines, StandardCharsets.UTF_8);
			LOGGER.info("Wrote config to file {}", configFilePath);
		}
		catch(IOException e) {
			LOGGER.error(e.getMessage(),  e);
		}
	}

	private void deleteFile(String path) throws IOException {
		Path configFilePath = Paths.get(path);
		boolean res = Files.deleteIfExists(configFilePath);
		LOGGER.info("Deleted existing file {} ={}", path, res);
	}

	/**
	 * Runs the Maude program for the given WorkPackage.<p>
	 * Program name and query(ies)/command(s) are taken from the WorkPackage. A "quit" command is automatically added at the end.
	 */
	private String runMaudeProgram(WorkPackage workPackage) {
		String executable = getSysDependentMaudeExecutable();

		final String maudeCall = getMaudePath() + executable;
		String usecaseAttribute = (String) workPackage.getData().get(USE_CASE_ATTRIBUTE_KEY);
		Usecase usecase = usecaseAttribute == null || usecaseAttribute.trim().isEmpty() ? null : Usecase.valueOf(usecaseAttribute);

		List<String> initialMaudeParameters;
		if(JOHAN_SVERDRUP == usecase) {
			initialMaudeParameters = new ArrayList<>();
		}
		else {
			initialMaudeParameters = prepareUsecase1MaudeCall(usecase);
		}
		LOGGER.info("Resulting Maude call: {} {}", maudeCall, initialMaudeParameters);

		final List<String> followUpCommands = prepareFollowupCommands(workPackage, usecase);

		String shellResult = ShellHelper.runShellCommand(maudeCall, initialMaudeParameters, followUpCommands.toArray(new String[0]));

		if(JOHAN_SVERDRUP == usecase) {
			// salvage history files
			shellResult += readHistoryFile(usecase, JS_HISTORY_FILENAME_1);
			shellResult += readHistoryFile(usecase, JS_HISTORY_FILENAME_2);
		}

		return shellResult;
	}

	private String readHistoryFile(Usecase usecase, String filename) {
		try {
			String directory = getMaudeProgramPath() + getMaudeUsecaseSubdir(usecase);
			List<String> contents = Files.readAllLines(Paths.get(directory + filename));
			StringBuilder sb = new StringBuilder();
			sb.append(System.lineSeparator());
			sb.append(filename + ':' + System.lineSeparator());
			for(String line : contents) {
				sb.append(System.lineSeparator());
				sb.append(line);
			}
			return sb.toString();
		}
		catch(IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
		return "";
	}

	/**
	 * Returns the path of the Maude runtime installation depending on the environment (if Docker is active or not).
	 */
	protected String getMaudePath() {
		boolean dockerActive = isDockerActive();
		LOGGER.info("Docker-mode={}", dockerActive);

		String maudePath = "";
		if(dockerActive) {
			maudePath = MAUDE_PATH_DOCKER;
		}
		else {
			maudePath = MAUDE_PATH_LOCAL;
		}
		LOGGER.info("Path of Maude runtime directory: {}", maudePath);

		return maudePath;
	}

	private List<String> prepareUsecase1MaudeCall(Usecase usecase) {
		List<String> maudeParameters = Arrays.asList(
				getMaudeProgramPath() + getMaudeUsecaseSubdir(usecase) + "petroleum.maude",
				getMaudeProgramPath() + getMaudeUsecaseSubdir(usecase) + CONFIG_FILENAME);
		return maudeParameters;
	}

	/**
	 * Returns the path of the Maude program depending on the environment (if Docker is active or not).
	 */
	protected String getMaudeProgramPath() {
		boolean dockerActive = isDockerActive();
		LOGGER.info("Docker-mode={}", dockerActive);

		String maudeProgramPath = "";
		if(dockerActive) {
			maudeProgramPath = MAUDE_PROGRAM_PATH_DOCKER;
		}
		else {
			maudeProgramPath = MAUDE_PROGRAM_PATH_LOCAL;
		}
		LOGGER.info("Path of GeoAssistant Maude program: {}", maudeProgramPath);

		return maudeProgramPath;
	}

	protected String getMaudeUsecaseSubdir(Usecase usecase) {
		if(usecase == null) {
			usecase = USECASE_1;
		}

		switch(usecase) {
			case JOHAN_SVERDRUP: {
				LOGGER.info("Usecase: {} --> Maude usecase-subdir is: {}", usecase, MAUDE_USECASE_SUBDIR_JS);
				return MAUDE_USECASE_SUBDIR_JS;
			}
			case USECASE_1:
			default: {
				LOGGER.info("Usecase: {} --> Maude usecase-subdir is: {}", usecase, MAUDE_USECASE_SUBDIR_SIS);
				return MAUDE_USECASE_SUBDIR_SIS;
			}
		}
	}

	/**
	 * Prepares all follow-up commands for the Maude program given in the WorkPackage (if any) by replacing the scenario-placeholder with the correct scenario
	 * designation used in this configuration.<p>
	 * The prepared commands/queries are then returned as Strings.
	 */
	private List<String> prepareFollowupCommands(WorkPackage workPackage, Usecase usecase) {
		if(usecase == null) {
			usecase = USECASE_1;
		}

		final List<String> followUpCommands;
		if(JOHAN_SVERDRUP == usecase) {
			followUpCommands = new ArrayList<>();
			followUpCommands.add("cd " + getMaudeProgramPath() + getMaudeUsecaseSubdir(usecase));
			followUpCommands.add("in import-all.maude");
			followUpCommands.add("in import-all.maude");
			followUpCommands.add("in " + CONFIG_FILENAME);
			followUpCommands.add("in proto-scenario-generation.maude");
			followUpCommands.add("erew init .");
			followUpCommands.add("in edvard-grieg-proto.maude");
			followUpCommands.add("erew protoScenario .");
		}
		else {
			String caseStudyName = (String) workPackage.getData().get(CONFIG_FILENAME_ATTRIBUTE_KEY);
			caseStudyName = caseStudyName.replace(MAUDE_FILE_SUFFIX, "");

			followUpCommands = new ArrayList<>(1);
			followUpCommands.add(CASE_STUDY_WILDCARD_PATTERN.matcher("rew {%scenario%} .").replaceAll(Matcher.quoteReplacement(caseStudyName)));
		}

		followUpCommands.add(MAUDE_QUIT_COMMAND);
		return followUpCommands;
	}

	/**
	 * Returns true if the application runs in a Docker environment (based on a system property that must be set in that case).
	 */
	private boolean isDockerActive() {
		String dockerFlag = System.getProperties().getProperty("docker");
		return dockerFlag == null || dockerFlag.trim().isEmpty() ? false : Boolean.parseBoolean(dockerFlag);
	}

	/**
	 * Returns the correct name of the Maude runtime executable depending on the operating system.<p>
	 * Currently only Linux and Mac are supported!
	 */
	protected String getSysDependentMaudeExecutable() {
		Properties prop = System.getProperties();
		String os =  prop.getProperty("os.name");

		String executable = "";
		if (os.startsWith("Linux") ) {
			executable = MAUDE_EXECUTABLE_LINUX;
		}
		else if (os.startsWith("Mac") ) {
			executable = MAUDE_EXECUTABLE_MAC;
		}
		else if (os.startsWith("Windows") ) {
			throw new UnsupportedOperationException("Windows OS is currently not supported.");
		}
		else {
			throw new UnsupportedOperationException("Unsupported operating system.");
		}

		LOGGER.info("Returned Maude executable: {}", executable);
		return executable;
	}

	/**
	 * Implements the results accumulation of the GeoAssistant application.<p>
	 * Due to performance considerations the final configurations discovered by the simulations are saved to a database.
	 * Instead of the actual data this method returns the database key of the ComputationRun that holds the information in the DB and the statistical data collected about this run's performance.<p>
	 * This method also persists the statistics data to the database and removes the results from the system to enable the next computation run to start.
	 */
	@PostMapping("/accumulateResults")
	public ResponseEntity<ComputationResult> accumulateResults(@RequestBody ResultsProtocol protocol) {
		LOGGER.info("Accumulating scenario results");

		Map<String, Object> resultData = new HashMap<>();
		long runId = protocol.getWorkPackageResults().get(0).getWorkPackage().getRunId();
		resultData.put("computationRunId", runId);

		addTimingResults(protocol, resultData);
		saveTimingDataToDB(protocol, runId);

		final ComputationResult result = new ComputationResult(Status.DONE, resultData);
		LOGGER.info("Accumulation done");
		return ResponseEntity.ok().body(result);
	}

	/**
	 * Persists the statistical data from the given ResultsProtocol to the given runId in the database.
	 */
	private void saveTimingDataToDB(@RequestBody ResultsProtocol protocol, long runId) {
		ComputationRun run = computationRunRepository.getOne(runId);
		if(run == null) {
			LOGGER.info("ComputationRun {} not found while collecting results", runId);
		}
		else {
			run.setComputingTime(protocol.getFinishedTimestamp() - protocol.getStartedTimestamp());
			run.setPreparationTime(protocol.getPreparationTime());
			run.setNumberNodesStart(protocol.getNumberNodesStart());
			run.setNumberNodesEnd(protocol.getNumberNodesEnd());
			run.setNumberWPs(protocol.getNumberWPs());
			run.setFastestWP(protocol.getMinWpTime());
			run.setSlowestWP(protocol.getMaxWpTime());
			run.setAvgWP(protocol.getAvgWpTime());

			computationRunRepository.save(run);
			LOGGER.info("Persisted timing data");
		}
	}

}
