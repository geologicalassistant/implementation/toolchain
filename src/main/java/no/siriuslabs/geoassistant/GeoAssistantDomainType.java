package no.siriuslabs.geoassistant;

import no.siriuslabs.computationapi.api.model.computation.DomainType;

/**
 * DomainType implementation for the GeoAssistant application.
 */
// used reflectively only
public enum GeoAssistantDomainType implements DomainType {

	GEO_ASSISTANT;

	@Override
	public String getDomainType() {
		return name();
	}

}
