package no.siriuslabs.geoassistant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static no.siriuslabs.geoassistant.GeoAssistantImplementationController.PYTHON_CONFIG_DIRECTORY;
import static no.siriuslabs.geoassistant.GeoAssistantImplementationController.CONFIG_FILENAME;
import static no.siriuslabs.geoassistant.GeoAssistantImplementationController.PYTHON_PATH_LOCAL;

/**
 * Helper class to convert Maude code to a state that can be used in JSON and back again.<p>
 * The conversion to JSON removes special characters such as double quotes, tabs and new line and either escapes, removes or replaces them with placeholders.
 * On the way back to Maude-usable code the escapes and placeholders are replaced by the original characters again.
 */
public class MaudeFileConverter {

	public enum DataType {
		MAUDE,
		ONTOLOGY;
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(MaudeFileConverter.class);

	/**
	 * Placeholder character for newline characters and linebreaks.
	 */
	private static final String NEWLINE_PLACEHOLDER = "§";

	/**
	 * Regex pattern to identify double quotes.
	 */
	private static final Pattern DOUBLE_QUOTE_PATTERN = Pattern.compile("\"", Pattern.LITERAL);
	/**
	 * Regex pattern to identify tabs.
	 */
	private static final Pattern TAB_PATTERN = Pattern.compile("\t", Pattern.LITERAL);
	/**
	 * Regex pattern to identify escaped double quotes.
	 */
	private static final Pattern ESCAPED_DOUBLE_QUOTE_PATTERN = Pattern.compile("\\\"", Pattern.LITERAL);

	private MaudeFileConverter() {
	}

	/**
	 * Main method for access to the conversion as a command line tool to convert initial Maude configurations or ontology files into something that can be used by a tool that can send JSON to a webservice.<p>
	 * For Maude configuration the tool expects the source file in the /config directory and with the same filename as used by the Python tool when generating scenarios.<p>
	 * For ontologies the input/output file needs to be as second parameter.
	 */
	public static void main(String[] args) {
		try {
			if(args.length == 0) {
				LOGGER.error("MaudeFileConverter needs at least 1 argument: MAUDE or ONTOLOGY. For the latter another one with the input/output path is required.");
				return;
			}

			final String filepath;
			DataType dataType = DataType.valueOf(args[0]);
			if(DataType.MAUDE == dataType) {
				filepath = PYTHON_PATH_LOCAL + PYTHON_CONFIG_DIRECTORY + '/' + CONFIG_FILENAME;
			}
			else if(DataType.ONTOLOGY == dataType) {
				if(args.length < 2) {
					LOGGER.error("MaudeFileConverter needs the input/output path to convert ontology files.");
					return;
				}
				filepath = args[1];
			}
			else {
				// should be impossible as mapping unknown enum elements produces an error
				return;
			}

			Path configFile = Paths.get(filepath);
			List<String> readLines = Files.readAllLines(configFile, StandardCharsets.UTF_8);

			List<String> linesToWrite = convertMaudeToJson(readLines);

			// write back to file without line separators
			try(FileWriter writer = new FileWriter(new File(filepath))) {
				for(String line : linesToWrite) {
					writer.append(line);
				}
			}
		}
		catch(IOException e) {
			LOGGER.error(e.getMessage(), e);
		};
	}

	/**
	 * Converts the given lines of Maude code into a version usable with JSON.<p>
	 * Double quotes are escaped, tabs removed and newlines replaced with the placeholder character.
	 * @return The lines given as parameters but changed as described above.
	 */
	public static List<String> convertMaudeToJson(List<String> readLines) {
		List<String> linesToWrite = new ArrayList<>(readLines.size());

		// remove tabulators and escape double quotes
		for(String line : readLines) {
			String modified = line;
			if(line.contains("\"")) {
				modified = DOUBLE_QUOTE_PATTERN.matcher(modified).replaceAll(Matcher.quoteReplacement("\\\""));
			}
			if(line.contains("\t")) {
				modified = TAB_PATTERN.matcher(modified).replaceAll(Matcher.quoteReplacement("  "));
			}

			linesToWrite.add(modified + NEWLINE_PLACEHOLDER);
		}
		return linesToWrite;
	}

	/**
	 * Converts the given JSON stream contents back to Maude code.<p>
	 * Newline placeholders are removed and escaped characters are "de-escaped".
	 * @return A list of source code lines in a Maude-usable form and without newline characters at the end.
	 * These can be easily added in a way automatically correct for the current OS by using Files.write() from the java.nio package.
	 */
	public static List<String> convertJsonToMaude(String configData) {
		List<String> lines = new ArrayList<>();

		StringTokenizer tokenizer = new StringTokenizer(configData, NEWLINE_PLACEHOLDER);
		while(tokenizer.hasMoreTokens()) {
			String line = tokenizer.nextToken();
			String modified = line;
			if(line.contains("\\\"")) {
				// de-escape double quotes
				modified = ESCAPED_DOUBLE_QUOTE_PATTERN.matcher(modified).replaceAll(Matcher.quoteReplacement("\""));
			}

			lines.add(modified);
		}
		return lines;
	}

}
